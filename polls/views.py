from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render,get_object_or_404

from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.utils import timezone

from django.http import HttpResponseRedirect
from .models import Question,Choice

# Create your views here.

class IndexView(ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by("-pub_date")[
        :3
    ]

class DetailView(DetailView):
    template_name ="polls/detail.html"
    model = Question
    """
        Excludes any questions that aren't published yet.
        """

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte = timezone.now())

class ResultsView(DetailView):
    model = Question
    template_name ='polls/results.html'

def vote(request, question_id):
    question = get_object_or_404(Question,pk = question_id)

    try:
        selected_choice = question.choice_set.get(pk = request.POST["choice"])
    except(KeyError, Choice.DoesNotExist):

        context ={"question":question,
              "error_message": "you did'not selection a choice",
              }
        return render(
             request,'polls/detail.html',context)
    
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse("polls:results", args=(question.id,)))

